#Argon GRUB Linux theme
#version:17
#lang:id_ID.UTF-8
#themeInputDir:/usr/share/dde-api/data/grub-themes/deepin
#screenWidth:1366
#screenHeight:768
#head end
title-text: ""
desktop-image: "background.jpg"
desktop-color: "#000000"
terminal-font: "Unifont Regular 14"
terminal-box: "terminal_box_*.png"
terminal-left: "0"
terminal-top: "0"
terminal-width: "100%"
terminal-height: "100%"
terminal-border: "0"
+ boot_menu {
    left = 28%
    top = 27%
    width = 597
    height = 351
    item_font = "Noto Sans CJK SC Regular 12"
    item_color = "#dddddd"
    selected_item_color = "#ffffff"
    item_height = 36
    item_spacing = 8
    item_padding = 8
    icon_width = 26
    icon_height = 18
    item_icon_space = 15
    item_pixmap_style = "item_*.png"
    selected_item_pixmap_style = "selected_item_*.png"
    menu_pixmap_style = "menu_*.png"
    scrollbar_thumb = "scrollbar_thumb_*.png"
    scrollbar_width = 6
}
+ label {
    left = 0
    top = 97%
    width = 100%
    align = "center"
    id = "__timeout__"
    color = "#99E53E"
    font = "Noto Sans CJK SC Regular 12"
    text = "Booting in %d seconds"
}
+ label {
    left = 0
    top = 94%
    width = 100%
    align = "center"
    color = "#99E53E"
    font = "Noto Sans CJK SC Regular 12"
    text = "Use ↑ and ↓ keys to change selection, Enter to confirm, E to edit the commands before booting or C for a command-line"
}
