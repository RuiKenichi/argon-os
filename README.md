# Argon OS

Welcome to Argon OS. Here, you will find editions of Arch Linux that come which components which vary with Vanilla Arch Linux.

[This is a fork of the ALG project (Arch Linux GUI ZEN)](https://github.com/arch-linux-gui/arch-linux-gui-zen)

###### RELEASE CYCLE: Whenever ready, and not on the first of every month. Also, not with the mainline ISO.
---
#### How To Build This Source Code
- Clone this git repo :
  `git clone https://gitlab.com/RuiKenichi/argon-os.git`

  `cd argon-os`

- Install required package :
  `sudo pacman -S archiso`

  `sudo pacman -S base-devel`

  `sudo pacman-key --recv-key FBA220DFC880C036 --keyserver keyserver.ubuntu.com`

  `sudo pacman-key --lsign-key FBA220DFC880C036`

  `sudo pacman -U  'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-keyring.pkg.tar.zst'  'https://cdn-mirror.chaotic.cx/chaotic-aur/chaotic-mirrorlist.pkg.tar.zst'`

- Backup Pacman Configuration

  `sudo cp /etc/pacman.conf /etc/pacman.conf.old`

  `sudo cp pacman.conf /etc/pacman.conf`

  `sudo pacman -Syyu`

- Build The ISO

  `sudo mkarchiso -v .`

- Output ISO is in the out folder

- Restore pacman configurations and remove chaotic-keyring and chaotic-mirrorlist if needed

  `sudo cp /etc/pacman.conf.old /etc/pacman.conf`

  `sudo pacman -Rns chaotic-keyring chaotic-mirrorlist`

> ### Build iso has been completed

---
###  Argon OS
###### Checksum: `The iso hasn't been built yet`
`NOTE: Internet Connection is required to install this edition`

The Argon OS differs from the Vanilla editions due to the inclusion of the following core components:
- The Linux Zen Kernel (Increased Responsiveness)
- Zsh as default shell
- btrFS as default File System (Automatic Partition Subvolumes: `/, /home, /var/cache, /var/log`)
- Z standard compression for ISO image (Lightning fast installation)
- Chaotic AUR Repository (Install AUR packages from terminal)

## Features
- KDE Plasma 5 on LiveCD (25th Anniversary Edition)
- Install Stock Desktop of Choice (with option for i3WM as well)
- Install Applications of Choice (Browser, Multimedia, Developement, etc)
- Network info copied over to future system (auth after reboot)
- Network Managed for Xfce, MATE, LxQt (applet)
- NVIDIA Driver Support
- Mesa Driver Support
- Multilib Enabled
- PAMAC Software Center
- SDDM default with Arch Linux Wallpaper
- Tap-to-click on Laptop touchpads
- Vulkan Devel Packages
- Pipewire With WirePlumber

## Installation
Press Window's key to launch application menu and search for **installer** or **Arch Linux**, and while `Install Arch Linux` is selected, press enter to launch the installer.

![](image/install.png)
The installer will launch and you should get something like this:
![](image/installer.png)

For partitioning with above mentioned subvolume scheme, select **Erase Disk**
![](image/btrfs.png)

`IMPORTANT`: You should select **Erase Disk** only if you are installing to one HDD/SSD, and wish to install Arch Linux on the entire disk. In all other cases, including implementation of your own subvolume scheme, **Manual Partioning** should be used.